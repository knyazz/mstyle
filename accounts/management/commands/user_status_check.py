# -*- coding: utf-8 -*-
from django.core.management import BaseCommand

from accounts.utils import check_user_status


class Command(BaseCommand):
    help = 'WMS Xml-file parser daemon. Use: ./manage.py user_status_check'

    def handle(self, *args, **options):
        check_user_status()
        return 'done'
        