#coding: utf-8
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserChangeForm
User = get_user_model()


class UsrCrtForm(forms.ModelForm):
    class Meta:
        model = User
        fields = 'email', 'fio', 'org', 'phone', 'company'

    def save(self, commit=True):
        user = super(UsrCrtForm, self).save(commit=False)
        if user.company:
            user.uin = user.company.uin
        else:
            user.uin = user.generate_uin
        user.username = user.auth_username
        user.set_password(user.uin)
        if commit:
            user.save()
        return user


class UsrChngForm(UserChangeForm):
    class Meta:
        model = User