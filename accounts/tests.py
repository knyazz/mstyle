from base.tests import BaseTest


class AccountsAPITest(BaseTest): 
    def accounts_simple_tests(self):
        '''
            test accounts functionality
        '''
        self.logining()
        #create
        creates = {
                    'api:accounts_v1:registration':   {
                                                    'fio': "2323 23",
                                                    #'password': 123,
                                                    'email': '123@123.ru',
                                                    'uuid': 123456789012345,
                                                    'phone': '123'
                                                },
                    'api:accounts_v1:pay_order':   {
                                                    'uuid': 123456789012345,
                                                    'fio': "2323 23",
                                                    #'password': 123,
                                                    'email': '123@123.ru',
                                                    'phone': '123'
                                                },
        }
        for url, params in creates.items():
            self.create_page(url, params)