from django.conf.urls import patterns, url
from django.contrib.auth import views

template_name = {'template_name': 'rest_framework/login.html'}

urlpatterns = patterns('accounts.views',
    url(r'^registration/$', 'registration', name='registration'),
    url(r'^login/$', 'api_login', name='login'),
    url(r'^logout/$', views.logout, template_name, name='logout'),
    url(r'^pay_order/$', 'pay_order', name='pay_order'),
)