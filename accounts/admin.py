from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DefaultUserAdmin
from django.utils.translation import ugettext_lazy as _

from base.admin import BaseAdmin

from .forms import UsrChngForm, UsrCrtForm
from .models import User, Company


class UserAdmin(DefaultUserAdmin):
    form = UsrChngForm
    add_form = UsrCrtForm
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': (
                                            'fio',
                                            'uin',
                                            'email',
                                            'phone',
                                            'org',
                                            'company',
                                            'is_active',
                                            'is_confirmed',
                                )}),
        (_('Permissions'), {'fields': ('is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ( 'email',
                        'fio',
                        'org',
                        'company',
                        'phone',
                    ),
        }),
    )
    list_display= ( 'id', 'fio', 'email', 'uin', 'company', 'phone',
                    'is_staff', 'is_active', 'is_confirmed')
    list_filter = ( 'is_staff', 'is_superuser', 'is_active', 'is_confirmed',
                    'groups', 'fio', 'email', 'username', 'uin', 'phone', 'org',
                    'company')
    readonly_fields = 'uin',
    ordering = '-id',
admin.site.register(User, UserAdmin)

admin.site.register(Company, BaseAdmin)