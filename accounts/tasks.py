#coding: utf-8
from __future__ import unicode_literals

from celery.task import periodic_task
from celery.schedules import crontab

from mstyle.celery import app

from .utils import check_user_status


@app.task(ignore_result=True)
@periodic_task(ignore_result=True, run_every=crontab(hour=0, minute=0), task_started=True)
def periodic_check_user():
    b'''
        Основная celery функция проверки пользователей.
    '''
    check_user_status()