# coding: utf-8
from __future__ import unicode_literals
import random
from dateutil.relativedelta import relativedelta

from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from django.utils.timezone import now

from base.models import TitleBaseModel


class Company(TitleBaseModel):
    uin = models.CharField(b'UIN Компании', max_length=255, unique=True)
    class Meta:
        verbose_name=b'Компания'
        verbose_name_plural=b'Компании'


class MstyleUserManager(UserManager):
    def create_mstyle_user(self, **kwargs):
        _uin = kwargs.pop('uin', None) or self.generate_uin()
        kwargs['uin'] = _uin
        _email = kwargs.pop('email', None)
        username = kwargs.pop('username', None) or _email or _uin
        password = kwargs.pop('password', None) or _uin
        return self._create_user(username,_email,password,False,False,**kwargs)

    def generate_username(self, email):
        return email or self.generate_uin()

    def generate_uin(self):
        _uin = 'V'+str(random.randint(12345,99999))
        if self.uin_exists(_uin):
            return self.generate_uin()
        return _uin

    def uin_exists(self, uin=None):
        return self.all().filter(uin=uin).exists()


class User(AbstractUser):
    objects = MstyleUserManager()
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = 'email', 'fio'
    fio = models.CharField(b'ФИО', max_length=1024, blank=True)
    uin = models.CharField(b'UIN', max_length=255, blank=True)
    uuid = models.CharField(b'UUID', max_length=255, blank=True)
    imei = models.CharField(b'IMEI', max_length=16, blank=True)
    org = models.CharField(b'Организация', max_length=255, blank=True)
    phone = models.CharField(b'Телефон', max_length=255, blank=True)
    is_confirmed = models.BooleanField(b'Подтвержденный', default=False)
    company = models.ForeignKey(Company, null=True, blank=True,
                                verbose_name=Company._meta.verbose_name)

    class Meta:
        ordering = '-id', '-date_joined',
        verbose_name=b'Пользователь'
        verbose_name_plural=b'Пользователи'

    def save(self, *args, **kwargs):
        if not self.pk:
            self.uin = self.generate_uin
        super(User, self).save(*args, **kwargs)

    @property
    def is_online(self):
        u''' проверка на возможность доступа к системе '''
        return self.uin and self.is_confirmed or self.check_date_joined_valid

    @property
    def check_date_joined_valid(self):
        cur_date = now()+relativedelta(months=-2)
        return self.is_active and self.date_joined > cur_date

    @property
    def generate_uin(self):
        if not self.pk and not self.uin:
            self.uin = 'V'+str(random.randint(12345,99999))
            if self.uin_exists:
                return self.generate_uin
        return self.uin

    @property
    def uin_exists(self):
        return self._meta.model.objects.filter(uin=self.uin).exists()

    @property
    def auth_username(self):
        return self.username or self.email or self.uin


def generate_uin():
    _uin = 'V'+str(random.randint(12345,99999))
    if User.objects.filter(uin=_uin).exists():
        return generate_uin()
    return _uin