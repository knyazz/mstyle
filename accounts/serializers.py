#coding: utf-8
from __future__ import unicode_literals
import re

from django.core import validators
from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model
User = get_user_model()

from rest_framework import serializers

from base.models import SystemTunes
from base.messages import DEFAULT_MESSAGES

from .models import Company
from .utils import CreateUserMixin


class NaturalIntegerField(serializers.CharField):
    def from_native(self, value):
        value = super(NaturalIntegerField, self).from_native(value)
        if value in validators.EMPTY_VALUES: return None
        if not re.match(r'^\d+$', value):
            raise ValidationError(u'Введите натуральное число')
        return value


class RegSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True)
    phone = serializers.CharField(min_length=1, max_length=255, required=True)
    fio = serializers.CharField(min_length=1, max_length=1024, required=True)
    #imei = NaturalIntegerField(required=True, min_length=15, max_length=16)
    class Meta:
        model = User
        fields = 'email', 'uin', 'fio', 'org', 'phone', 'id'#, 'uuid'
        #write_only_fields = 'uuid',
        read_only_fields = 'id', 'uin'

    def validate_email(self, attrs, source):
        value = attrs[source]
        user = User.objects.filter(email=value).last()
        if user:
            self.user_is_valid(user)
        return attrs

    def user_is_valid(self, user):
        if user.is_confirmed:
            raise serializers.ValidationError(DEFAULT_MESSAGES.get(0))
        elif user.check_date_joined_valid:
            raise serializers.ValidationError(DEFAULT_MESSAGES.get(1))
        else:
            raise serializers.ValidationError(DEFAULT_MESSAGES.get(2))
        error_message=b'Пользователь с таким email уже существует'
        raise serializers.ValidationError(error_message)

    def validate_uuid(self, attrs, source):
        value = attrs[source]
        user = User.objects.filter(uuid=value).last()
        if user:
            error_message=b'Пользователь с таким uuid уже существует'
            raise serializers.ValidationError(error_message)
        return attrs


class AuthSerializer(CreateUserMixin, serializers.ModelSerializer):
    email = serializers.EmailField(required=True)
    uin = serializers.CharField(min_length=1, max_length=255, required=True)
    class Meta:
        model = User
        fields = 'uin', 'email', 'fio'
        #write_only_fields = 'uin',

    def get_email_user(self, email):
        if hasattr(self, '_email_user') and self._email_user.email==email:
            return self._email_user
        return User.objects.filter(email=email).last()

    def get_uin_user(self, uin, email=None):
        if hasattr(self, '_uin_user') and self._uin_user.uin==uin:
            return self._uin_user
        if email:
            return User.objects.filter(uin=uin).filter(email=email).last()
        else:
            return User.objects.filter(uin=uin).last()

    def validate_uin(self, attrs, source, email_exists=False):
        value = attrs[source]
        new_user = self.check_user_by_company_uin(attrs, value)
        if new_user:
            _view = self.context.get('view')
            _view._new_user = new_user
            return attrs
        user = self.get_uin_user(value, email=attrs.get('email'))
        print user
        if user:
            self.check_user_by_uin(attrs, user)
        elif not user:
            error_message=b'Пользователь не существует с таким UIN'
            raise serializers.ValidationError(error_message)
        elif not user.is_online:
            error_message=b'Пользователь заблокирован с таким UIN'
            st = SystemTunes.objects.all().last()
            if st and st.auth_block_msg:
                error_message = st.auth_block_msg
            raise serializers.ValidationError(error_message)
        return attrs

    def check_user_by_uin(self, attrs, user):
        # проверяем пользователя по uin
        if user:
            self.check_user_by_email(attrs, source='email', uin_user=user)
        else:
            self.check_user_by_email(attrs, source='email') 
        return attrs

    def check_user_by_company_uin(self, attrs, uin):
        _company = Company.objects.filter(uin=uin).last()
        _email = attrs.get('email')
        _user = self.get_email_user(_email)
        if _company and _email and not _user:
            attrs['company'] = _company
            attrs['uin'] = _company.uin
            self._new_user = self.create_mstyle_user(**attrs)
            return self._new_user


    def check_user_by_email(self, attrs, source, uin_user=False):
        #данная валидация идет, после проверки uin
        _email = attrs[source]
        __email = hasattr(uin_user, 'email') and getattr(uin_user, 'email')
        user = __email == _email and uin_user
        if user:
            if uin_user:
                #email и uin существуют, не клиент М-стайл
                if not user.is_confirmed and not user.check_date_joined_valid:
                    #срок временного использования истек
                    raise serializers.ValidationError(DEFAULT_MESSAGES.get(2))
            else:
                #email существует, uin нет
                raise serializers.ValidationError(DEFAULT_MESSAGES.get(3))
        elif uin_user and _email:
            #EMAIL не сущестует, uin - да, проверяем клиент ли М-стайл
            if uin_user.is_confirmed and not uin_user.email:
                #да, клиент М-стайл
                # обновляем данные пользователя
                #ЭТО ОЧЕНЬ НЕБЕЗАПАСНО, НО ЗАКАЗЧИК ТАК ЗАХОТЕЛ
                #НЕОБХОДИМО УБРАТЬ В БУДУЩЕМ
                #return attrs
                try:
                    User.objects.filter(pk=uin_user.pk).update(**attrs)
                except:
                    errmsg=b'Регистрация клиента М-стайла без email не прошла'
                    raise serializers.ValidationError(errmsg)
            else:
                #email не сущестует и uin не клиент М-стайл
                raise serializers.ValidationError(DEFAULT_MESSAGES.get(4))
        return attrs

    def get_validation_exclusions(self, instance=None):
        excl = super(AuthSerializer, self).get_validation_exclusions(instance)
        excl.append('uin')
        return excl