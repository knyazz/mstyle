#coding: utf-8
from dateutil.relativedelta import relativedelta

from django.contrib.auth import get_user_model, login, authenticate
from django.utils.timezone import now
User = get_user_model()

from rest_framework.response import Response

from base.models import EmailTemplate
from base.utils import generate_email


def check_user_status():
    cur_date = now()+relativedelta(months=-2)
    User.objects.filter(date_joined__lte=cur_date).update(is_active=False)


class CreateUserMixin(object):
    def send_email(self, obj):
        email_tpl = EmailTemplate.objects.filter(tpl_type=0).last()
        if email_tpl:
            bctx = dict(user=obj, password=obj.uin)
            msg = generate_email( 
                        subject=email_tpl.subject,
                        to = obj.email,
                        body_tpl = email_tpl.text,
                        body_context = bctx,
                    )
            msg.send()

    def create_mstyle_user(self, **data):
        _usr = User.objects.create_mstyle_user(**data)
        self.send_email(_usr)
        return _usr


class LoginMstyleUser(object):
    def auth_logining(self, user, uin, request):
        if user and uin:
            user = authenticate(username=user.username,password=uin)
            if user:
                login(request, user)
                return Response({'success': True, 'id': user.id}, status=200)
            else:
                return Response({'uin': b'Некорректный uin',}, status=400)