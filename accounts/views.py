#coding: utf-8
from __future__ import unicode_literals
from django.conf import settings
from django.contrib.auth import get_user_model, authenticate, login
User = get_user_model()

from rest_framework import generics
from rest_framework.response import Response

from base.models import EmailTemplate
from base.tasks import send_email
from base.views import BaseAPI

from .serializers import RegSerializer, AuthSerializer
from .utils import CreateUserMixin, LoginMstyleUser


class Registration(CreateUserMixin, BaseAPI, generics.CreateAPIView):
    model = User
    serializer_class = RegSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.DATA, files=request.FILES)
        if serializer.is_valid():
            self.object = self.create_mstyle_user(**serializer.data)
            self.post_save(self.object, created=True)
            serializer.data['id'] = self.object.id
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=201, headers=headers)        
        return Response(serializer.errors, status=400)

    def post_save(self, obj, created=False):
        user = authenticate(username=obj.username,password=obj.uin)
        if user: login(self.request, user)
registration = Registration.as_view()


class AuthLogin(LoginMstyleUser, BaseAPI, generics.GenericAPIView):
    model = User
    serializer_class = AuthSerializer

    def post(self, request, *args, **kwargs):
        return self.login(request, *args, **kwargs)

    def login(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.DATA, files=request.FILES)
        if serializer.is_valid():
            _uin = serializer.data['uin']
            _email = serializer.data['email']
            _objects = User.objects.filter(uin=_uin, email=_email)
            self.object = _objects.last()
            if _objects.count() > 1:
                if hasattr(self, '_new_user'):
                    self.object = self._new_user
                    _uin = self._new_user.uin
            response = self.auth_logining(self.object, _uin, request)
            if response:
                return response
        return Response(serializer.errors, status=400)
api_login = AuthLogin.as_view()


class PayOrder(BaseAPI, generics.CreateAPIView):
    model = User
    serializer_class = RegSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.DATA, files=request.FILES)
        if serializer.is_valid():
            # sent email
            self.send_email(request.user)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=201, headers=headers)        
        return Response(serializer.errors, status=400)

    def send_email(self, obj):
        email_tpl = EmailTemplate.objects.filter(tpl_type=1).last()
        if email_tpl:
            bctx = dict(user=obj)
            send_email.delay( 
                        subject=email_tpl.subject,
                        to = settings.DEFAULT_TO_EMAIL,
                        body_tpl = email_tpl.text,
                        body_context = bctx,
                    )
pay_order = PayOrder.as_view()