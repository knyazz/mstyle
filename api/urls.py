from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    url(r'^1/accounts/', include('accounts.urls', namespace='accounts_v1')),
    url(r'^latest/accounts/', include('accounts.urls', namespace='accounts_latest')),

    url(r'^1/base/', include('base.urls', namespace='base_v1')),
    url(r'^latest/base/', include('base.urls', namespace='base_latest')),
)