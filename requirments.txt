python-dateutil
psycopg2
six
raven
Pillow

#celery
billiard
pytz
kombu
amqp
anyjson
celery


Django==1.7
django-cors-headers
djangorestframework==2.4.3
#django-filer
django-mptt
django-suit
django-select2
#django-ckeditor-updated

#git+https://github.com/mariocesar/sorl-thumbnail.git

#development
ipython
#ipdb==0.8

#dev_tools
nose
nose-notify
django-nose
#pyinotify==0.9.4
#django-dynamic-fixture==1.6.5
#factory_boy==2.2.1
#requests==2.1.0

#docs
Sphinx
Pygments
docutils
Jinja2
MarkupSafe
django-documentation