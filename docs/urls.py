from django.conf.urls import patterns, url, include
from django.shortcuts import redirect


index_redirect = lambda r: redirect('/docs/python/index.html')

urlpatterns = patterns(
    'docs',
    url(r'^python/$', index_redirect),
    url(r'^python/', include('documentation.urls')),
)