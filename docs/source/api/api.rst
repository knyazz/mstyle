API mstyle version 1.0
===========================

.. toctree::
    :maxdepth: 1

    authentification


Базовые запросы к API
=================================

**Список новостей в системе**
    | методы: GET
    | django-имя: ``{% url 'api_verion_1:news_list' %}``
    | url: /api/version/1/base/news_list/

Пример ответа::

        [
            {
                "id": 1, 
                "title": "100 \u043c\u0431\u0438\u0442", 
                "desc": "1", 
                "created": "2014-09-22T15:17:26.454Z"
            },
            {
                "id": 2, 
                "title": "100 \u043c\u0431\u0438\u0442", 
                "desc": "1", 
                "created": "2014-09-22T15:17:26.454Z"
            }
        ]

где title - название, id - id, desc - описание, created - время создания в формате ISO-8601

**Новость в системе**
    | методы: GET
    | django-имя: ``{% url 'api_verion_1:news_detail' id %}``
    | url: /api/version/1/base/news/<id>/

Пример ответа::
        {
            "id": 1, 
            "title": "100 \u043c\u0431\u0438\u0442", 
            "desc": "1", 
            "created": "2014-09-22T15:17:26.454Z"
        }

где title - название, id - id, desc - описание, created - время создания в формате ISO-8601

**Список семинаров в системе**
    | методы: GET
    | django-имя: ``{% url 'api_verion_1:seminar_list' %}``
    | url: /api/version/1/base/seminar_list/

Пример ответа::

        [
            {
                "id": 1, 
                "title": "100 \u043c\u0431\u0438\u0442", 
                "desc": "123", 
                "created": "2014-09-19T06:25:10.336Z", 
                "is_actual": true, 
                "start_time": "2014-09-19T10:25:08Z", 
                "end_time": "2014-09-19T10:25:09Z",
                "tp": null,
                "is_applied": false,
            },
            {
                "id": 2, 
                "title": "100 \u043c\u0431\u0438\u0442", 
                "desc": "123", 
                "created": "2014-09-19T06:25:10.336Z", 
                "is_actual": true, 
                "start_time": "2014-09-19T10:25:08Z", 
                "end_time": "2014-09-19T10:25:09Z",
                "tp": {"id": 1, "title": "name"},
                "is_applied": true,
            }
        ]

где title - название, id - id, desc - описание, created - время создания, is_actual - действующий ли семинар, start_time - время начала семинара, end_time - время окончание семинара в формате ISO-8601, tp - вид семинара, is_applied - записан ли текущий пользователь на семинар.


**Список семинаров в системе, на которые подписан пользователь**
    | методы: GET
    | django-имя: ``{% url 'api_verion_1:user_seminar_list' %}``
    | url: /api/version/1/base/user_seminar_list/

Пример ответа::

        [
            {
                "id": 1,
            },
            {
                "id": 2,
            }
        ]

где id - id

**Список медиафайлов**
    | методы: GET
    | django-имя: ``{% url 'api_verion_1:media_files_list' %}``
    | url: /api/version/1/base/media_files_list/
    | GET-параметры: fl_type - фильтр по типу файлов

Пример ответа::

        [
            {
                "file_url": "/media/skate.jpg", 
                "id": 1, 
                "title": "100 \u043c\u0431\u0438\u0442", 
                "desc": "1", 
                "created": "2014-09-18T11:57:57.715Z", 
                "fl": "./skate.jpg", 
                "fl_type": 0
            }
        ]

где title - название, id - id, desc - описание, created - время создания в формате ISO-8601, fl_type - тип файлов (0 - аудио, 1 -видео, 2 - документ), file_url - путь до файла

**Список Подборок документов**
    | методы: GET
    | django-имя: ``{% url 'api_verion_1:media_gallery_list' %}``
    | url: /api/version/1/base/media_gallery_list/

Пример ответа::

        [
            {
                "files": [
                    {
                        "file_url": "/media/skate.jpg", 
                        "id": 1, 
                        "title": "100 \u043c\u0431\u0438\u0442", 
                        "desc": "1", 
                        "created": "2014-09-18T11:57:57.715Z", 
                        "fl": "./skate.jpg", 
                        "fl_type": 0
                    }, 
                    {
                        "file_url": "/media/temp/logo_1394541045.png", 
                        "id": 2, 
                        "title": "2", 
                        "desc": "222", 
                        "created": "2014-09-22T16:59:03.227Z", 
                        "fl": "temp/logo_1394541045.png", 
                        "fl_type": 1
                    }
                ], 
                "id": 1, 
                "title": "100 \u043c\u0431\u0438\u0442", 
                "desc": "1", 
                "created": "2014-09-18T11:58:11.374Z"
            }
        ]

где title - название, id - id, desc - описание, created - время создания в формате ISO-8601, files - список медиафайлов

**Краткий Список Подборок документов**
    | методы: GET
    | django-имя: ``{% url 'api_verion_1:simple_gallery_list' %}``
    | url: /api/version/1/base/simple_gallery_list/

Пример ответа::

        [
            {
                "id": 1, 
                "title": "100 \u043c\u0431\u0438\u0442", 
                "desc": "1", 
                "created": "2014-09-18T11:58:11.374Z"
            }
        ]

где title - название, id - id, desc - описание, created - время создания в формате ISO-8601


**Заказ документа на email**
    | методы: POST
    | django-имя: ``{% url 'api_verion_1:email_file_order_create' %}``
    | url: /api/version/1/base/email_file_order_create/

Пример запроса::

        {
            "client": 1,
            "name": "qqwe",
            "number": "qweasd",
            "org": "asdf"
        }

где name - название документа, number - номер документа, org - принявший орган, client - id пользователя-заказчика. Все поля обязательные. При успешной обработке отдает HTTP status 201.


**Заказ документа**
    | методы: POST
    | django-имя: ``{% url 'api_verion_1:file_order_create' %}``
    | url: /api/version/1/base/file_order_create/

Пример запроса::

        {
            "client": 1,
            "fl": 1,
        }

где fl - id файла, client - id пользователя-заказчика. Все поля обязательные. При успешной обработке отдает HTTP status 201.


**Заказ на подборку документов**
    | методы: POST
    | django-имя: ``{% url 'api_verion_1:gallery_order_create' %}``
    | url: /api/version/1/base/gallery_order_create/

Пример запроса::

        {
            "client": 1,
            "gallery": 1,
        }

где gallery - id подборки документов из Списка подборок документов, client - id пользователя-заказчика. Все поля обязательные. При успешной обработке отдает HTTP status 201.

**Запись на семинар**
    | методы: POST
    | django-имя: ``{% url 'api_verion_1:seminar_order_create' %}``
    | url: /api/version/1/base/seminar_order_create/

Пример запроса::

        {
            "client": 1,
            "seminar": 1,
        }

где seminar - id семинара, client - id пользователя-заказчика. Все поля обязательные. При успешной обработке отдает HTTP status 201.