import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
SECRET_KEY = '08g&iog#4h1mj%izb5d$aq_aoq2%#86te59(x*f!+i4=8k*83*'
PROJECT_NAME = 'mstyle'
DEBUG = True
TEMPLATE_DEBUG = True
ALLOWED_HOSTS = []

AUTH_USER_MODEL = 'accounts.User'

INSTALLED_APPS = (
    'suit',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'corsheaders',
    'documentation',
    #'filer',
    'rest_framework',
    'django_select2',
    #'sorl.thumbnail',

    'accounts',
    'base',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    'corsheaders.middleware.CorsMiddleware',

    'base.middleware.DisableCSRF'
)

ROOT_URLCONF = 'mstyle.urls'

WSGI_APPLICATION = 'mstyle.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        'ATOMIC_REQUESTS': True,
    }
}

DEFAULT_FROM_EMAIL = 'noreply@mstyle.indev-group.eu'
DEFAULT_TO_EMAIL = 'hot@mstyle.indev-group.eu'

DOCUMENTATION_HTML_ROOT = os.path.join(BASE_DIR, 'docs/_build/html')
DOCUMENTATION_ROOT = os.path.join(BASE_DIR, 'docs')
DOCUMENTATION_ACCESS_FUNCTION = lambda user: user.is_staff
DOCUMENTATION_XSENDFILE = False


LANGUAGE_CODE = 'ru'
LANGUAGES = (('ru','Russian'),)
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
)

CELERY_ACCEPT_CONTENT = ('pickle', 'json', 'msgpack', 'yaml')
BROKER_URL = 'amqp://guest:guest@localhost:5672//'

CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST = ()
CORS_ORIGIN_REGEX_WHITELIST = ()
CORS_URLS_REGEX = r'^/api/.*$'

CORS_ALLOW_METHODS = (
    'GET',
    'POST',
    'OPTIONS',
)

CORS_ALLOW_HEADERS = (
    'x-http-method-override',
    'x-requested-with',
    'x-csrftoken',
    'content-type',
    'accept',
    'origin',
    'authorization',
)

CORS_PREFLIGHT_MAX_AGE = 86400
CORS_ALLOW_CREDENTIALS = True

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'
CKEDITOR_UPLOAD_PATH = 'uploads/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, "templates"),
)

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
NOSE_ARGS = [
             '--no-start-message',
             '--verbosity=2',
             '--with-fixture-bundling',
            ]

SUIT_CONFIG = {
    'ADMIN_NAME': 'M-style',
    'CONFIRM_UNSAVED_CHANGES': True,
    'MENU_EXCLUDE': ('sites',),
}

try:
    from local_settings import *
except ImportError:
    pass