#coding: utf-8
from django.conf import settings
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from rest_framework import generics, exceptions

from .models import GalleryOrder,  FileOrder, MediaModel, MediaGallery, News
from .models import Seminar, SeminarOrder, EmailTemplate, EmailFileOrder
from .models import SeminarType, SystemTunes

from .exceptions import PermissionMstyleDenied
from .permissions import APIBasePerm

from .serializers import GOSerializer, FOSerializer, SOSerializer
from .serializers import MGSerializer, MFSerializer, SeminarSerializer
from .serializers import SimpleMGSerializer, SimpleSeminarSerializer

from .tasks import send_email


class BaseAPI(object):
    @method_decorator(csrf_exempt)
    def post(self, *args, **kwargs):
        return super(BaseAPI, self).post(*args, **kwargs)

class BaseWithPerm(BaseAPI):
    permission_classes = APIBasePerm,

    def permission_denied(self, request):
        if not request.successful_authenticator:
            raise exceptions.NotAuthenticated()
        if request.user.is_authenticated() and not request.user.is_online:
            st = SystemTunes.objects.all().last()
            if st and st.auth_block_msg:
                raise PermissionMstyleDenied(detail=st.auth_block_msg)
            raise PermissionMstyleDenied()
        raise exceptions.PermissionDenied()


class OrderCreateBase(BaseWithPerm):
    def send_email(self, obj, email_tpl_type):
        email_tpl = EmailTemplate.objects.filter(tpl_type=email_tpl_type).last()
        if email_tpl:
            bctx = dict(order=obj)
            send_email.delay( 
                        subject=email_tpl.subject,
                        to = settings.DEFAULT_TO_EMAIL,
                        body_tpl = email_tpl.text,
                        body_context = bctx,
                    )


class FileOrderCreate(OrderCreateBase, generics.CreateAPIView):
    model = FileOrder
    serializer_class = FOSerializer

    def post_save(self, obj, created=False):
        self.send_email(obj, email_tpl_type=4)
file_order_create = FileOrderCreate.as_view()


class EmailFileOrderCreate(OrderCreateBase, generics.CreateAPIView):
    model = EmailFileOrder

    def post_save(self, obj, created=False):
        self.send_email(obj, email_tpl_type=4)
email_file_order_create = EmailFileOrderCreate.as_view()


class GalleryOrderCreate(OrderCreateBase, generics.CreateAPIView):
    model = GalleryOrder
    serializer_class = GOSerializer

    def post_save(self, obj, created=False):
        self.send_email(obj, email_tpl_type=3)
gallery_order_create = GalleryOrderCreate.as_view()


class SeminarOrderCreate(OrderCreateBase, generics.CreateAPIView):
    model = SeminarOrder
    serializer_class = SOSerializer

    def post_save(self, obj, created=False):
        self.send_email(obj, email_tpl_type=2)
seminar_order_create = SeminarOrderCreate.as_view()


class MediaFilesList(BaseWithPerm, generics.ListAPIView):
    model = MediaModel
    serializer_class = MFSerializer

    def filter_queryset(self, queryset):
        qs = super(MediaFilesList, self).filter_queryset(queryset)
        if self.request.QUERY_PARAMS.getlist('fl_type'):
            fl_types = self.request.QUERY_PARAMS.getlist('fl_type')
            return qs.filter(fl_type__in=fl_types)
        return qs
media_files_list = MediaFilesList.as_view()


class MediaGalleryList(BaseWithPerm, generics.ListAPIView):
    model = MediaGallery
    serializer_class = MGSerializer
media_gallery_list = MediaGalleryList.as_view()


class SimpleMGList(MediaGalleryList):
    serializer_class = SimpleMGSerializer
simple_gallery_list = SimpleMGList.as_view()


class SeminarTypeList(generics.ListAPIView):
    model = SeminarType
seminar_types = SeminarTypeList.as_view()


class SeminarList(BaseWithPerm, generics.ListAPIView):
    model = Seminar
    serializer_class = SeminarSerializer
    def filter_queryset(self, queryset):
        qs = super(SeminarList, self).filter_queryset(queryset)
        return qs

    def get_user_lst(self, user):
        qs = self.get_queryset()
        lst = set(qs.filter(seminarorder__client=user
                    ).values_list('pk',flat=True)
        )
        return qs.filter(pk__in=lst)
seminar_list = SeminarList.as_view()


class UserSeminarList(SeminarList):
    serializer_class = SimpleSeminarSerializer
    def filter_queryset(self, queryset):
        qs = super(UserSeminarList, self).filter_queryset(queryset)
        user = self.request.user
        lst = set(qs.filter(seminarorder__client=user
                    ).values_list('pk',flat=True)
        )
        return qs.filter(pk__in=lst)
user_seminar_list = UserSeminarList.as_view()


class NewsList(generics.ListAPIView):
    model = News
    #serializer_class = NewsSerializer
news_list = NewsList.as_view()


class NewsDetail(generics.RetrieveAPIView):
    model=News
news_detail = NewsDetail.as_view()