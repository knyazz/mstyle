#coding: utf-8
from rest_framework.permissions import BasePermission


class APIBasePerm(BasePermission):
    u'Проверка на доступ к API'
    def has_permission(self, request, view):
        return request.user.is_authenticated() and request.user.is_online