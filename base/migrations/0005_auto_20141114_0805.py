# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0004_auto_20141030_0730'),
    ]

    operations = [
        migrations.CreateModel(
            name='SeminarType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'\xd0\x9d\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5')),
            ],
            options={
                'verbose_name': '\u0422\u0438\u043f \u0441\u0435\u043c\u0438\u043d\u0430\u0440\u0430',
                'verbose_name_plural': '\u0422\u0438\u043f\u044b \u0441\u0435\u043c\u0438\u043d\u0430\u0440\u0430',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='seminar',
            name='cost',
            field=models.CharField(default=b'\xd0\x91\xd0\xb5\xd1\x81\xd0\xbf\xd0\xbb\xd0\xb0\xd1\x82\xd0\xbd\xd1\x8b\xd0\xb9', max_length=255, verbose_name=b'\xd0\xa1\xd1\x82\xd0\xbe\xd0\xb8\xd0\xbc\xd0\xbe\xd1\x81\xd1\x82\xd1\x8c'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='seminar',
            name='tp',
            field=models.ForeignKey(verbose_name=b'\xd0\xa2\xd0\xb8\xd0\xbf \xd1\x81\xd0\xb5\xd0\xbc\xd0\xb8\xd0\xbd\xd0\xb0\xd1\x80\xd0\xb0', blank=True, to='base.SeminarType', null=True),
            preserve_default=True,
        ),
    ]
