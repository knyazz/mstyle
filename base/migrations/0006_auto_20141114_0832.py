# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0005_auto_20141114_0805'),
    ]

    operations = [
        migrations.CreateModel(
            name='SystemTunes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('auth_block_msg', models.TextField(verbose_name=b'\xd0\xa1\xd0\xbe\xd0\xbe\xd0\xb1\xd1\x89\xd0\xb5\xd0\xbd\xd0\xb8\xd0\xb5 \xd0\xbe \xd0\xb1\xd0\xbb\xd0\xbe\xd0\xba\xd0\xb8\xd1\x80\xd0\xbe\xd0\xb2\xd0\xba\xd0\xb5')),
            ],
            options={
                'verbose_name': '\u0421\u0438\u0441\u0442\u0435\u043c\u043d\u044b\u0435 \u043d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438',
                'verbose_name_plural': '\u0421\u0438\u0441\u0442\u0435\u043c\u043d\u044b\u0435 \u043d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='seminartype',
            options={'verbose_name': '\u0412\u0438\u0434 \u0441\u0435\u043c\u0438\u043d\u0430\u0440\u0430', 'verbose_name_plural': '\u0412\u0438\u0434\u044b \u0441\u0435\u043c\u0438\u043d\u0430\u0440\u0430'},
        ),
        migrations.AlterField(
            model_name='seminar',
            name='tp',
            field=models.ForeignKey(verbose_name=b'\xd0\x92\xd0\xb8\xd0\xb4 \xd1\x81\xd0\xb5\xd0\xbc\xd0\xb8\xd0\xbd\xd0\xb0\xd1\x80\xd0\xb0', blank=True, to='base.SeminarType', null=True),
        ),
    ]
