# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('base', '0003_auto_20141001_1525'),
    ]

    operations = [
        migrations.CreateModel(
            name='EmailFileOrder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'\xd0\xa1\xd0\xbe\xd0\xb7\xd0\xb4\xd0\xb0\xd0\xbd')),
                ('executed', models.BooleanField(default=False, verbose_name=b'\xd0\x98\xd1\x81\xd0\xbf\xd0\xbe\xd0\xbb\xd0\xbd\xd0\xb5\xd0\xbd\xd0\xbe')),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd0\x9d\xd0\xb0\xd0\xb7\xd0\xb2\xd0\xb0\xd0\xbd\xd0\xb8\xd0\xb5 \xd0\xb4\xd0\xbe\xd0\xba\xd1\x83\xd0\xbc\xd0\xb5\xd0\xbd\xd1\x82\xd0\xb0')),
                ('number', models.CharField(max_length=255, verbose_name=b'\xd0\x9d\xd0\xbe\xd0\xbc\xd0\xb5\xd1\x80 \xd0\xb4\xd0\xbe\xd0\xba\xd1\x83\xd0\xbc\xd0\xb5\xd0\xbd\xd1\x82\xd0\xb0')),
                ('org', models.CharField(max_length=255, verbose_name=b'\xd0\x9f\xd1\x80\xd0\xb8\xd0\xbd\xd1\x8f\xd0\xb2\xd1\x88\xd0\xb8\xd0\xb9 \xd0\xbe\xd1\x80\xd0\xb3\xd0\xb0\xd0\xbd')),
                ('client', models.ForeignKey(verbose_name=b'\xd0\x9f\xd0\xbe\xd0\xbb\xd1\x8c\xd0\xb7\xd0\xbe\xd0\xb2\xd0\xb0\xd1\x82\xd0\xb5\xd0\xbb\xd1\x8c', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': '\u0417\u0430\u043a\u0430\u0437 \u0434\u043e\u043a\u0443\u043c\u0435\u043d\u0442\u0430 \u043d\u0430 email',
                'verbose_name_plural': '\u0417\u0430\u043a\u0430\u0437\u044b \u0434\u043e\u043a\u0443\u043c\u0435\u043d\u0442\u043e\u0432 \u043d\u0430 email',
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='fileorder',
            options={'verbose_name': '\u0417\u0430\u043a\u0430\u0437 \u0434\u043e\u043a\u0443\u043c\u0435\u043d\u0442\u0430', 'verbose_name_plural': '\u0417\u0430\u043a\u0430\u0437\u044b \u0434\u043e\u043a\u0443\u043c\u0435\u043d\u0442\u043e\u0432'},
        ),
    ]
