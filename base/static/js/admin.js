$(function() {
  if (typeof CKEDITOR !== 'undefined') {
    //CKEDITOR.config.contentsCss = '/static/css/style.css';
    $('div.cke_contents').attr('style', 'height: 400px;');

    CKEDITOR.on('dialogDefinition', function (ev) {
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;
        dialogDefinition.minHeight =100;
        dialogDefinition.minWidth = 400;
        var editor = ev.editor;

        dialogDefinition.removeContents('Link');
        dialogDefinition.removeContents('advanced');

        if (dialogName == 'image') {
            var infoTab = dialogDefinition.getContents('info');

            dialogDefinition.onOk = function (e) {
                var imageSrcUrl = e.sender.originalElement.$.src;
                var width = e.sender.originalElement.$.width;
                var height = e.sender.originalElement.$.height;
                var txtHeight = dialogDefinition.dialog.getValueOf("info", "txtHeight");
                var txtWidth = dialogDefinition.dialog.getValueOf("info", "txtWidth");
                if (txtHeight) {
                  height = txtHeight;
                };
                if (txtWidth) {
                  width = txtWidth;
                };
                var imgHtml = CKEDITOR.dom.element.createFromHtml('<a class=\"fancybox\" href=\"' + imageSrcUrl + '\"><img src=' + imageSrcUrl + ' width=\"' + width + '\" height=\"' + height + '\" alt=\"\" /></a>');
                editor.insertElement(imgHtml);
            };

            infoTab.remove('txtHSpace');
            infoTab.remove('txtVSpace');
            infoTab.remove('txtBorder');
            //infoTab.remove('txtHeight');
            //infoTab.remove('txtWidth');
            infoTab.remove('cmbAlign');
            infoTab.remove('ratioLock');

            infoTab.get('htmlPreview').style = 'display: none;'
        }
    });
  }
});

$(document).ready(function () {
    $('div.cke_contents').attr('style', 'height: 400px;');
});

$(function() {
  var fupl = $('.file-upload');
  fupl.each( function(index,value) {
    var fl = value.getElementsByTagName('a');
    if (fl && fl[0].getAttribute('href')) {
      var inputs = value.getElementsByTagName('input');
      if (inputs && inputs[0].attributes.required) {
        inputs[0].attributes.removeNamedItem('required');
      }
    }
  });
});