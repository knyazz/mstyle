from django.conf import settings
from django.core.mail import EmailMessage
from django.template import Context, Template


def generate_email(subject,to,body_tpl,body_context,from_email=None):
    from_email = from_email or settings.DEFAULT_FROM_EMAIL
    body_tpl = Template(body_tpl)
    body = body_tpl.render(Context(body_context))
    msg = EmailMessage( subject=subject, to=(to,), body=body,
                        from_email=from_email)
    return msg