#coding: utf-8
from __future__ import unicode_literals
from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models

from .choices import MEDIA_FILETYPES, EMAIL_TEMPLATES


class TitleBaseModel(models.Model):
    title = models.CharField(b'Название', max_length=255,)
    __unicode__ = lambda self: self.title
    class Meta:
        abstract=True


class BaseModel(TitleBaseModel):
    desc = models.TextField(b'Описание', blank=True)
    created = models.DateTimeField(b'Создан', auto_now_add=True)

    class Meta:
        abstract=True


class SystemTunes(models.Model):
    auth_block_msg = models.TextField(b'Сообщение о блокировке пользователя')
    class Meta:
        verbose_name= b'Системные настройки'
        verbose_name_plural = verbose_name


class MediaModel(BaseModel):
    fl = models.FileField(b'Файл', upload_to='temp')
    fl_type = models.PositiveSmallIntegerField(b'Тип файла', 
                                                choices=MEDIA_FILETYPES)
    class Meta:
        verbose_name=b'Медиафайл'
        verbose_name_plural = b'Медиафайлы'

    @property
    def absolute_admin_url(self):
        if self.pk:
            return reverse('admin:base_mediamodel_change', args=(self.pk,),)


class MediaGallery(BaseModel):
    files = models.ManyToManyField(MediaModel, null=True, blank=True,
                            verbose_name=MediaModel._meta.verbose_name_plural)
    class Meta:
        verbose_name=b'Подборка документов'
        verbose_name_plural=b'Подборки документов'

    @property
    def absolute_admin_url(self):
        if self.pk:
            return reverse('admin:base_mediagallery_change', args=(self.pk,),)


class GalleryOrder(models.Model):
    created = models.DateTimeField(b'Создан', auto_now_add=True)
    executed = models.BooleanField(b'Исполнено', default=False)
    gallery = models.ForeignKey(MediaGallery,
                                verbose_name=MediaGallery._meta.verbose_name)
    client = models.ForeignKey(settings.AUTH_USER_MODEL,
                                verbose_name=b'Пользователь')
    __unicode__ = lambda self: 'Заказ {0} от {1} для {2}'.format(  self.pk, 
                                                                    self.created,
                                                                    self.client)
    class Meta:
        verbose_name=b'Заказ на подборку документов'
        verbose_name_plural=b'Заказы на подборку документов'


class FileOrder(models.Model):
    created = models.DateTimeField(b'Создан', auto_now_add=True)
    executed = models.BooleanField(b'Исполнено', default=False)
    fl = models.ForeignKey(MediaModel,
                            verbose_name=MediaModel._meta.verbose_name)
    client = models.ForeignKey(settings.AUTH_USER_MODEL,
                                verbose_name=b'Пользователь')
    __unicode__ = lambda self: 'Заказ {0} от {1} для {2}'.format(  self.pk, 
                                                                    self.created,
                                                                    self.client)
    class Meta:
        verbose_name=b'Заказ документа'
        verbose_name_plural=b'Заказы документов'


class EmailFileOrder(models.Model):
    created = models.DateTimeField(b'Создан', auto_now_add=True)
    executed = models.BooleanField(b'Исполнено', default=False)
    client = models.ForeignKey(settings.AUTH_USER_MODEL,
                                verbose_name=b'Пользователь')
    name = models.CharField(b'Название документа', max_length=255)
    number = models.CharField(b'Номер документа', max_length=255, blank=True)
    org = models.CharField(b'Принявший орган', max_length=255, blank=True)
    __unicode__ = lambda self: 'Заказ {0} от {1} для {2}'.format(  self.pk, 
                                                                    self.created,
                                                                    self.client)
    class Meta:
        verbose_name=b'Заказ документа на email'
        verbose_name_plural=b'Заказы документов на email'

    @property
    def absolute_admin_url(self):
        if self.pk:
            return reverse('admin:base_emailfileorder_change', args=(self.pk,),)


class SeminarType(TitleBaseModel):
    class Meta:
        verbose_name=b'Вид семинара'
        verbose_name_plural=b'Виды семинара'


class Seminar(BaseModel):
    is_actual=models.BooleanField(b'Актуально', default=True)
    start_time = models.DateTimeField(b'Время начала')
    end_time = models.DateTimeField(b'Время окончания')
    cost = models.CharField(b'Стоимость', default=b'Бесплатный',
                            max_length=255)
    tp = models.ForeignKey(SeminarType, null=True, blank=True,
                            verbose_name=SeminarType._meta.verbose_name)
    @property
    def absolute_admin_url(self):
        if self.pk:
            return reverse('admin:base_seminar_change', args=(self.pk,),)

    class Meta:
        verbose_name=b'Семинар'
        verbose_name_plural=b'Семинары'


class SeminarOrder(models.Model):
    seminar = models.ForeignKey(Seminar,
                                verbose_name=Seminar._meta.verbose_name)
    client = models.ForeignKey(settings.AUTH_USER_MODEL,
                                verbose_name=b'Пользователь')
    created = models.DateTimeField(b'Создан',auto_now_add=True)
    __unicode__ = lambda self: 'Заказ {0} от {1} для {2}'.format(  self.pk, 
                                                                    self.created,
                                                                    self.client)
    class Meta:
        verbose_name=b'Запись на семинар'
        verbose_name_plural=b'Записи на семинары'


class News(BaseModel):
    class Meta:
        verbose_name=b'Новости'
        verbose_name_plural=b'Новости'


class EmailTemplate(models.Model):
    subject = models.CharField(b'Заголовок', max_length=255)
    text = models.TextField(b'Текст сообщения')
    tpl_type = models.PositiveSmallIntegerField(b'Тип шаблона',
                            unique=True,
                            choices=EMAIL_TEMPLATES)
    class Meta:
        verbose_name=b'Шаблон email сообщений'
        verbose_name_plural=b'Шаблоны email сообщений'