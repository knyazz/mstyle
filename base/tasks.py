#coding: utf-8
from mstyle.celery import app

from .utils import generate_email


@app.task(ignore_result=True)
def send_email(subject, to, recipients=None, **kwargs):
    u'''
        Основная celery функция асинхронной отправки email сообщений.

        :param subject: Тема письма
        :type subject: str
        :param to: Заголовок to письма.
        :type to: list
        :param from_email: Адресат письма. По умолчанию `settings.DEFAULT_FROM_EMAIL`
        :type from_email: str
        :param recipients: Список получателей. Если указан,
            то рассылает по одному письму всем получателям,
            соответственно заполняя поле to письма
        :type recipients: list
        :param body_tpl: шаблона письма
        :type body_tpl_path: str
        :param body_context: Контекст шаблона письма
        :type body_context: dict
    '''
    if recipients:
        for r in recipients:
            msg = generate_email(subject, to=[r], **kwargs)
            msg.send()
    else:
        msg = generate_email(subject, to, **kwargs)
        msg.send()