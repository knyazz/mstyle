#coding: utf-8
import rest_framework

class PermissionMstyleDenied(rest_framework.exceptions.PermissionDenied):
    default_detail = 'You do not have permission to perform this action.'