#coding: utf-8
from __future__ import unicode_literals


MEDIA_FILETYPES = (
    (0, b'Аудио'),
    (1, b'Видео'),
    (2, b'Документ'),
)

EMAIL_TEMPLATES = (
    (0, b'Регистрация'),
    (1, b'Запрос на оплату'),
    (2, b'Запись на семинар'),
    (3, b'Заказ подборки документов'),
    (4, b'Заказать документы на e-mail'),
)