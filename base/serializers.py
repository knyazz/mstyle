#coding: utf-8
from rest_framework import serializers

from .models import GalleryOrder,  FileOrder, MediaModel, MediaGallery
from .models import Seminar, SeminarOrder, SeminarType


class GOSerializer(serializers.ModelSerializer):
    class Meta:
        model = GalleryOrder


class FOSerializer(serializers.ModelSerializer):
    class Meta:
        model = FileOrder


class MFSerializer(serializers.ModelSerializer):
    file_url = serializers.Field(source="fl.url")
    class Meta:
        model = MediaModel


class SimpleMGSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaGallery
        fields = ('id', 'title', 'desc', 'created')

class MGSerializer(serializers.ModelSerializer):
    files = MFSerializer()
    class Meta:
        model = MediaGallery


class SeminarTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = SeminarType


class SeminarSerializer(serializers.ModelSerializer):
    tp = SeminarTypeSerializer()
    class Meta:
        model = Seminar

    def to_native(self, obj):
        data = super(SeminarSerializer, self).to_native(obj)
        data['is_applied'] = False
        _view = self.context.get('view')
        if _view:
            _user = self.context.get('request').user
            _user_lst = _view.get_user_lst(_user)
            data['is_applied'] = obj in _user_lst
        return data


class SimpleSeminarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Seminar
        fields = 'id',


class SOSerializer(serializers.ModelSerializer):
    class Meta:
        model = SeminarOrder