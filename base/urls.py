from django.conf.urls import patterns, url

urlpatterns = patterns('base.views',
    url(r'^email_file_order_create/$', 'email_file_order_create', name='email_file_order_create'),
    url(r'^file_order_create/$', 'file_order_create', name='file_order_create'),
    url(r'^gallery_order_create/$', 'gallery_order_create', name='gallery_order_create'),
    url(r'^seminar_order_create/$', 'seminar_order_create', name='seminar_order_create'),

    
    url(r'^media_files_list/$', 'media_files_list', name='media_files_list'),
    url(r'^media_gallery_list/$', 'media_gallery_list', name='media_gallery_list'),
    url(r'^simple_gallery_list/$', 'simple_gallery_list', name='simple_gallery_list'),
    url(r'^seminar_types/$', 'seminar_types', name='seminar_types'),
    url(r'^seminar_list/$', 'seminar_list', name='seminar_list'),
    url(r'^user_seminar_list/$', 'user_seminar_list', name='user_seminar_list'),
    url(r'^news_list/$', 'news_list', name='news_list'),
    url(r'^news/(?P<pk>\d+)/$', 'news_detail', name='news_detail'),
)